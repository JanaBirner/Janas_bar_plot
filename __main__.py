import os

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


BASE_DIR = os.path.dirname(os.path.realpath(__file__))
INPUT_DATA_DIR = os.path.join(BASE_DIR, "input_data_barplot")

# function with three variables that ultimately returns a pd DataFrame
def load_and_prepare_data(
    input_csv_data_file_name: str, columns_of_interest, name_mapper: dict
) -> pd.DataFrame:
    df = pd.read_csv(os.path.join(INPUT_DATA_DIR, input_csv_data_file_name))
    df = df[columns_of_interest].rename(columns=name_mapper)

    return df


# call function 5 times for each data set to prepare data
nd_gain_df = load_and_prepare_data(
    input_csv_data_file_name="nd_gain.csv",
    columns_of_interest=["ISO3", "2018"],
    name_mapper={"2018": "nd_gain", "ISO3": "ISO"},
)

regions_df = load_and_prepare_data(
    input_csv_data_file_name="regional_division.csv",
    columns_of_interest=["Regional bureaux", "ISO Code"],
    name_mapper={"Regional bureaux": "Region", "ISO Code": "ISO"},
)

IDP_df = load_and_prepare_data(
    input_csv_data_file_name="idmc_all_forbarplot.csv",
    columns_of_interest=["Conflict Stock Displacement", "ISO3","Disaster New Displacements", "Conflict New Displacements", "Year"],
    name_mapper={"Conflict Stock Displacement": "conflict_IDPs", "ISO3": "ISO",
                 "Conflict New Displacements": "conflict_displacements_flow",
                 "Disaster New Displacements": "disaster_displacements_flow"},
)


# Clean input data
nd_gain_df = nd_gain_df[nd_gain_df["ISO"].notna()]
IDP_df = IDP_df[IDP_df["ISO"].notna()]


merged = pd.merge(nd_gain_df, IDP_df, on="ISO")
df = pd.merge(merged, regions_df, on="ISO")

df.dropna(inplace=True)

# selecting rows based on condition
df = df.loc[df['Year'] > 2009]

#getting the average value
df = df.loc[df['ISO'] > 2009]


# output: cleaned data as csv file
df.to_csv("combined_data.csv")